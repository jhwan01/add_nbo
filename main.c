#include <stdio.h>
#include <stdint.h>

int main(int ac, char ** av)
{
	FILE *num1, *num2;
	uint8_t buf1[4] = {0,};
        uint8_t buf2[4] = {0,};
	uint32_t a, b;

	if(ac != 3)
	{
		printf("파일이 제대로 입력되지 않았습니다.");
		return 0;
	}
	num1 = fopen(av[1], "r");
	if (num1 == NULL)
	{
		printf("파일 1이 비어있음");
		return 0;
	}
	fread(buf1, sizeof(uint8_t), 4, num1);
	a = buf1[3] + buf1[2] * 256 + buf1[1] * 256 * 256 + buf1[1] * 256 * 256 *256;
	num2 = fopen(av[2], "r");
	if (num2 == NULL)
        {
                printf("파일 2이 비어있음");
                return 0;
        }
        fread(buf2, sizeof(uint8_t), 4, num2);
        b = buf2[3] + buf2[2] * 256 + buf2[1] * 256 * 256 + buf2[1] * 256 * 256 *256;
 	printf("%u(0X%x) + %u(0X%x) = %u(0X%x)", a, a, b, b, a+b, a+b);
	fclose(num1);
	fclose(num2);
	return 0;
}
