#Makefile
all: add_nbo

add_nbo: main.o
	gcc -o add-nbo main.o

main.o: main.c

clean:
	rm -f add-nbo
	rm -f *.o
